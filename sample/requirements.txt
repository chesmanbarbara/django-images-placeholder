Django==1.9.7
django-filer==1.3.0
django-mptt==0.8.7
django-polymorphic==1.3
easy-thumbnails==2.4.2
Pillow==5.0.0
Unidecode==0.4.21
